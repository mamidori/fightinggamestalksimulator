﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SFB;
using System.IO;

public class Read_PlayerImageFile : MonoBehaviour
{
    RawImage _gameobject_image;
    [SerializeField]
    private bool isright;
    Texture image;
    RectTransform _Rtransform;

    Animation _animation;

    bool is_highlight;

    Vector2 initialSize;
    // Start is called before the first frame update
    void Start()
    {
        _gameobject_image = GetComponent<RawImage>();
        _Rtransform = GetComponent<RectTransform>();
        _animation = GetComponent<Animation>();
        Reflash_Image();
        initialSize = new Vector2(1000,1000);

    }

    string OpenFile(){
        //画像を選択させる
        var extensions = new [] {
            new ExtensionFilter("Image Files", "png", "jpg", "jpeg" ),
            new ExtensionFilter("All Files", "*" ),
        };
        var imagepath = "";
        var paths = StandaloneFileBrowser.OpenFilePanel("Open File", "", extensions, true);
        for(var i=0;i<paths.Length;i++){
        Debug.Log(paths[i]);
        imagepath += paths[i];
        }
        return imagepath;
    }

    byte[] ReadFile(string imagepath){

        //バイナリに変更
        using (FileStream fileStream = new FileStream (imagepath, FileMode.Open, FileAccess.Read)) {
            BinaryReader bin = new BinaryReader (fileStream);
            byte[] values = bin.ReadBytes ((int)bin.BaseStream.Length);
            bin.Close ();
            return values;
        }
    }

    //テクスチャに変更
    Texture readByBinary(byte[] bytes) {
        Texture2D texture = new Texture2D (1, 1);
        texture.LoadImage (bytes);
        //var image_sprite = Sprite.Create(texture, new Rect(0,0,texture.width,texture.height), Vector2.zero);
        return texture;
    }

    public void ReadImage(){
        var imagepath = OpenFile();
        var bytes = ReadFile(imagepath);
        image = readByBinary(bytes);
        Set_thisImage(image);
        entry();
    }
    void Set_thisImage(Texture _image){
        
        _gameobject_image.texture = _image;
        if(_image != null){
            _gameobject_image.FixAspect();
        }
        

    }

    public void XFlip(){
        _Rtransform.eulerAngles += new Vector3(0f,180f,0f);
    }

    void entry(){
        if(isright){
            Sentence_Fluctuation.SetSentence_RightImage(image,_Rtransform.eulerAngles);
        }else{
            Sentence_Fluctuation.SetSentence_LeftImage(image,_Rtransform.eulerAngles);
        }
    }

    public void Reflash_Image(){
        Texture _nowimage;
        if(isright){
            _nowimage = Sentence_Fluctuation.Sentence_list[Sentence_Fluctuation.now_sentence].rightimage;
        }else{
            _nowimage = Sentence_Fluctuation.Sentence_list[Sentence_Fluctuation.now_sentence].leftimage;
        }
        Set_thisImage(_nowimage);
        Animation_Start();
    }

    void Animation_Start(){//TODO:AnimationのAnimationsを番号で取得できないのでとりあえずハードコーディング
        if(isright){
            if(!Sentence_Fluctuation.Sentence_list[Sentence_Fluctuation.now_sentence].is_leftplayer){
                if(!is_highlight){
                    _animation.Play("player2_highlight");
                    is_highlight = true;
                }
            }else{
                if(is_highlight){
                    _animation.Play("player2_NonActive");
                    is_highlight = false;
                }
            }
        }else{
            if(Sentence_Fluctuation.Sentence_list[Sentence_Fluctuation.now_sentence].is_leftplayer){
                if(!is_highlight){
                    _animation.Play("player1_highlight");
                    is_highlight = true;
                }
            }else{
                if(is_highlight){
                    _animation.Play("player1_NonActive");
                    is_highlight = false;
                }
            }

        }
    }

}
