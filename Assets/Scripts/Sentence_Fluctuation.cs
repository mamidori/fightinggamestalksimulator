﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class Sentence_Fluctuation : MonoBehaviour
{
    public static int now_sentence = 0;
    public int max_sentence = 0;
    TextMeshProUGUI _sentence_text;
    // Start is called before the first frame update

    [System.Serializable]
    public class Sentence{
        public Texture leftimage = null;
        public Texture rightimage = null;
        public Vector3 leftimage_vector = new Vector3();
        public Vector3 rightimage_vector = new Vector3();
        public string Text = "";
        public bool is_leftplayer = true;
    }

    
    public static List<Sentence> Sentence_list = new List<Sentence>();

    [SerializeField]
    List<Sentence> _sentence_list;
    void Start()
    {
        _sentence_text = transform.GetChild(0).gameObject.GetComponent<TextMeshProUGUI>();
        Sentence_list.Add(AddSentence());
    }

    void Update(){
        _sentence_list = Sentence_list;
    }
    public void right_sentence(){
        
        if(now_sentence==max_sentence){
            max_sentence++;
            Sentence_list.Add(AddSentence());
        }
        now_sentence++;
        Text_Refresh();
    }

    public void left_sentence(){
        now_sentence = (now_sentence>0) ? now_sentence-1 : 0;
        Text_Refresh();
    }

    void Text_Refresh(){
        string text = ""+(now_sentence+1)+"/"+(max_sentence+1);
        _sentence_text.SetText(text);
        
    }
    Sentence AddSentence(Texture limage = null,Texture rimage = null,
                                        Vector3 leftvec= new Vector3(),Vector3 rightvec = new Vector3(),
                                        string text = ""){
        Sentence _sentence = new Sentence();
        try{
            _sentence.leftimage = Sentence_list[now_sentence].leftimage;
            _sentence.rightimage = Sentence_list[now_sentence].rightimage;
            _sentence.is_leftplayer = Sentence_list[now_sentence].is_leftplayer;
        }catch{
            _sentence.leftimage = limage;
            _sentence.rightimage = rimage;
            _sentence.is_leftplayer = true;
        }

        _sentence.leftimage_vector = leftvec;
        _sentence.rightimage_vector = rightvec;
        _sentence.Text = text;
        
        return _sentence;
    }
    public static void SetSentence_RightImage(Texture rimage,Vector3 rightvec){
        Sentence_list[now_sentence].rightimage = rimage;
        Sentence_list[now_sentence].rightimage_vector = rightvec;
    }

    public static void SetSentence_LeftImage(Texture limage,Vector3 leftvec){
        Sentence_list[now_sentence].leftimage = limage;
        Sentence_list[now_sentence].leftimage_vector = leftvec;
    } 

    public static void SetSentence_Text(string text){
        Sentence_list[now_sentence].Text = text;
    } 
    public static void SetSentence_isLeftPlayer(bool is_left){
        Sentence_list[now_sentence].is_leftplayer = is_left;
    } 
}
