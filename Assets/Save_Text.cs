﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Save_Text : MonoBehaviour
{
    TextMeshProUGUI _tmprotext;
    [SerializeField]
    TMP_InputField _Inputfield;
    // Start is called before the first frame update
    void Start()
    {
        _tmprotext = GetComponent<TextMeshProUGUI>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Entry(){
        Sentence_Fluctuation.SetSentence_Text(_tmprotext.text);
    }
    public void Text_Call(){
        _Inputfield.text = Sentence_Fluctuation.Sentence_list[Sentence_Fluctuation.now_sentence].Text;
    }
}
