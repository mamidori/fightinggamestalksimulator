﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Select : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField]
    GameObject button1;
    [SerializeField]
    GameObject button2;

    bool left_is_highlight = true;
    void Start()
    {
        
    }

    public void Player1_button(){
        if(!left_is_highlight){
            Sentence_Fluctuation.SetSentence_isLeftPlayer(true);
            button2.SetActive(false);
            button1.SetActive(true);
            left_is_highlight = true;
        }else{
            Sentence_Fluctuation.SetSentence_isLeftPlayer(false);
            button2.SetActive(true);
            button1.SetActive(false);
            left_is_highlight = false;
        }

    }

    public void Player2_button(){
        if(left_is_highlight){
            Sentence_Fluctuation.SetSentence_isLeftPlayer(false);
            button2.SetActive(true);
            button1.SetActive(false);
            left_is_highlight = false;
        }else{
            Sentence_Fluctuation.SetSentence_isLeftPlayer(true);
            button2.SetActive(false);
            button1.SetActive(true);
            left_is_highlight = true;
        }
    }

    public void Visible_button(){
        if(Sentence_Fluctuation.Sentence_list[Sentence_Fluctuation.now_sentence].is_leftplayer){
            button2.SetActive(false);
            button1.SetActive(true);
            left_is_highlight = true;
        }else{
            button2.SetActive(true);
            button1.SetActive(false);
            left_is_highlight = false;
        }
    }
}
